package guice

import actors.SlaActor
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport
import services.SlaScheduler


class SlaSchedulerModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[SlaActor](SlaActor.SLA_ACTOR_NAME)
    bind(classOf[SlaScheduler]).asEagerSingleton()
  }
}