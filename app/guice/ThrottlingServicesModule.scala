package guice

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import net.codingwell.scalaguice.ScalaModule
import services._

class ThrottlingServicesModule extends AbstractModule with ScalaModule {
  override def configure(): Unit = {
    bind(classOf[SLAService])
      .annotatedWith(Names.named("direct"))
      .to(classOf[SLAServiceMockImpl])
      .asEagerSingleton()
    bind(classOf[SLAService])
      .annotatedWith(Names.named("cache"))
      .to(classOf[SLAServiceCachingImpl])
      .asEagerSingleton()
    bind(classOf[ThrottlingService])
      .to(classOf[ThrottlingServiceImpl])
      .asEagerSingleton()
  }
}
