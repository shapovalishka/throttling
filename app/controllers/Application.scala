package controllers

import com.google.inject.Inject
import play.api._
import play.api.mvc._
import services.ThrottlingService

import scala.concurrent.ExecutionContext

object Application {
  val AUTHORIZATION = "Authorization"
}

class Application @Inject() (throttling: ThrottlingService)(implicit ec: ExecutionContext) extends Controller {

  import Application._

  def withThrottling = Action.async { request =>
    val authHeader = request.headers.get(AUTHORIZATION)
    throttling.isRequestAllowed(authHeader).map { isAllowed =>
      if (isAllowed)
        Ok("Ok")
      else
        TooManyRequests(s"RSP limit is reached for ${authHeader.getOrElse("unauthorized")}")
    }
  }
}