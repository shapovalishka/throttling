package actors

import akka.actor.Actor
import akka.actor.Actor.Receive
import com.google.inject.{Inject, Singleton}
import services.SLAService.Sla
import services.ThrottlingConfiguration

import scala.collection.immutable

object SlaActor {

  val updateIntervalMillis = 100

  final val SLA_ACTOR_NAME = "sla-actor"

  case object Update

  case class CheckSla(sla: Sla)

  case class CheckSlaAnswer(allowed: Boolean)

  sealed case class SlaStats(defaultRps: Int,
                             autoScaleRps: Int,
                             currentRps: Int)

}

@Singleton
class SlaActor @Inject() (configuration: ThrottlingConfiguration) extends Actor {

  import SlaActor._

  override def receive: Receive = updateSla(immutable.Map.empty[String, SlaStats])

  private def updateSla(slaMap: immutable.Map[String, SlaStats]): Receive = {
    case Update =>
      context.become(updateSla(slaMap.mapValues { stats =>
        (if (stats.currentRps < stats.defaultRps)
          stats.copy(autoScaleRps = stats.defaultRps)
        else if (stats.currentRps < stats.autoScaleRps)
          stats
        else {
          val autoScale = stats.autoScaleRps.toDouble * configuration.autoScalePercents / 100
          stats.copy(autoScaleRps = stats.autoScaleRps + autoScale.ceil.toInt)
        }).copy(currentRps = 0)
      }))
    case CheckSla(sla) =>
      val slaStatsOpt = slaMap.get(sla.userName)
      context.become(updateSla(slaMap + (sla.userName -> slaStatsOpt.map { stats =>
        stats.copy(currentRps = stats.currentRps + 1)
      }.getOrElse {
        val updatesPerIntervalCoef: Double = updateIntervalMillis.toDouble / 1000
//        println(s"ooo -- $updatesPerIntervalCoef")
        SlaStats((sla.maxRps * updatesPerIntervalCoef).ceil.toInt, (sla.maxRps * updatesPerIntervalCoef).ceil.toInt, 1)
      })))
//      println(slaMap)
      sender() ! CheckSlaAnswer(slaStatsOpt.isEmpty || slaStatsOpt.exists(stats => stats.currentRps < stats.autoScaleRps))
  }
}
