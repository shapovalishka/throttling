package services

import java.util.concurrent.ConcurrentHashMap

import com.google.inject.Inject
import com.google.inject.name.Named
import play.api.cache.CacheApi
import services.SLAService.Sla

import scala.collection._
import scala.collection.convert.decorateAsScala._
import scala.collection.immutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success


object SLAService {
  case class Sla(userName:String, maxRps:Int)
}

trait SLAService {

  import SLAService._

  def getSlaByToken(token:String): Future[Sla]

}

class SLAServiceMockImpl @Inject() (implicit ec: ExecutionContext) extends SLAService {
  final val slaMocks = immutable.Map(
    "AA" -> Sla("A", 55),
    "AB" -> Sla("A", 55),
    "BB" -> Sla("B", 30),
    "CC" -> Sla("C", 15)
  )
  override def getSlaByToken(token: String): Future[Sla] = Future {
    slaMocks(token)
  }
}

class SLAServiceCachingImpl @Inject() (cache: CacheApi,
                                      @Named("direct") val slaService: SLAService,
                                       configuration: ThrottlingConfiguration
                                      )(implicit ec: ExecutionContext) extends SLAService {

  // SLA information is changed quite rarely and SLAService is quite costly to call,
  // so consider caching SLA requests. Also, you should not query the service,
  // if the same token request is already in progress.
  val slaMap: concurrent.Map[String, Future[Sla]] = new ConcurrentHashMap[String, Future[Sla]].asScala

  override def getSlaByToken(token: String): Future[Sla] =
    cache.get[Sla](token).map(Future.successful).getOrElse {
      lazy val slaF = slaService.getSlaByToken(token)
      val getSlaF = slaMap.get(token)
        .map(_.flatMap(_ =>  slaF).recoverWith {
          case _ => slaF
        })
        .getOrElse(slaF)
      slaMap.put(token, getSlaF)
      getSlaF onSuccess { case res => cache.set(token, res, configuration.cacheSlaHours.hours) }
      getSlaF
    }
}
