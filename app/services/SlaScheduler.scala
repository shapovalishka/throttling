package services

import actors.SlaActor
import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

class SlaScheduler @Inject() (val system: ActorSystem,
                              @Named(SlaActor.SLA_ACTOR_NAME) val slaActor: ActorRef,
                              configuration: ThrottlingConfiguration)(implicit ec: ExecutionContext) {

  system.scheduler.schedule(
    SlaActor.updateIntervalMillis.milliseconds,
    SlaActor.updateIntervalMillis.milliseconds,
    slaActor, SlaActor.Update)

}