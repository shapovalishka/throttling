package services

import com.google.inject.{Inject, ProvidedBy, Provider}
import com.typesafe.config.Config
import play.api.Configuration
import net.ceedubs.ficus.Ficus._

@ProvidedBy(classOf[ThrottlingConfigurationProvider])
case class ThrottlingConfiguration(graceRps: Int,
                                   awaitSlaResponseMilliseconds: Int,
                                   autoScalePercents: Int,
                                   cacheSlaHours: Int)

class ThrottlingConfigurationProvider @Inject() (config: Configuration) extends Provider[ThrottlingConfiguration] {
  override def get(): ThrottlingConfiguration = {
    val ns = config.underlying.as[Config]("throttling")
    ThrottlingConfiguration(
      graceRps = ns.as[Int]("grace_rps"),
      awaitSlaResponseMilliseconds = ns.as[Int]("await_sla_response_milliseconds"),
      autoScalePercents = ns.as[Int]("auto_scale_percents"),
      cacheSlaHours = ns.as[Int]("cache_sla_hours")
    )
  }
}

