package services

import actors.SlaActor
import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import services.SLAService.Sla
import akka.pattern._
import akka.util.Timeout
import com.google.inject.name.Named
import play.api.Logger

import scala.concurrent.{Await, ExecutionContext, Future, TimeoutException}
import scala.concurrent.duration._
import scala.util.control.NonFatal

object ThrottlingService {
  val UNAUTHORIZED_REQUESTS = "unauthorized_requests"
}

trait ThrottlingService {

  val GraceRps:Int

  val slaService: SLAService

  def isRequestAllowed(token:Option[String]): Future[Boolean]

}

class ThrottlingServiceImpl @Inject() (val system: ActorSystem,
                                       @Named("cache") override val slaService: SLAService,
                                       @Named(SlaActor.SLA_ACTOR_NAME) val slaActor: ActorRef,
                                       config: ThrottlingConfiguration)(implicit ec: ExecutionContext) extends ThrottlingService {

  import ThrottlingService._

  val log = Logger(getClass)

  override val GraceRps = config.graceRps

  private implicit val timeout = Timeout(config.awaitSlaResponseMilliseconds.milliseconds)
  private lazy val unauthorizedSla = Future.successful(Sla(UNAUTHORIZED_REQUESTS, GraceRps))

  private def getSlaByToken(token:String): Future[Sla] = {
    lazy val slaFuture = slaService.getSlaByToken(token)
    lazy val yetFuture = after(duration = config.awaitSlaResponseMilliseconds.milliseconds,
      using = system.scheduler)(unauthorizedSla)
    Future firstCompletedOf Seq(slaFuture, yetFuture)
  }

  override def isRequestAllowed(token: Option[String]): Future[Boolean] = {
    token.map(getSlaByToken(_).recoverWith {
      case NonFatal(e) => unauthorizedSla
    }).getOrElse(unauthorizedSla)
        .flatMap { sla =>
//          println(sla)
          (slaActor ? SlaActor.CheckSla(sla)).map {
            case SlaActor.CheckSlaAnswer(isAllowed) =>
//              println(s"isAllowes - $isAllowed")
              isAllowed
            case answer@_ =>
              log.warn(s"Sla actor answered wrong answer : ${answer} ")
              false
          }
        }
        .recover {
          case NonFatal(e) =>
            log.error(s"Sla check failed: ${e.getMessage}")
            false
        }
  }
}
