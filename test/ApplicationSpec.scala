import actors.SlaActor
import akka.actor.Status.Success
import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Names
import com.google.inject.util.Providers
import controllers.Application
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test._
import play.api.test.Helpers._
import services._
import play.api.inject.bind
import play.api.libs.concurrent.Akka
import play.api.mvc.AnyContentAsEmpty
import services.SLAService.Sla

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.duration._
import scala.util.Try


/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec(implicit ec: ExecutionContext) extends Specification {

  implicit val system = ActorSystem("test")
  val scheduler = system.scheduler

  val mocks = immutable.Map(
    "AA" -> Sla("A", 55),
    "AB" -> Sla("A", 55),
    "AC" -> Sla("A", 55),
    "BA" -> Sla("B", 30),
    "BB" -> Sla("B", 30),
    "CA" -> Sla("C", 15)
  )

  val time = 25 // seconds
  val acceptedRpsOverflowCoefficient = 1.15 // here we accept that RPS can overcome on 15% in average maximumRps for a big amout of requests
  val maxRequestsPerTime = (time * mocks.map(_._2.maxRps).toSet.sum * acceptedRpsOverflowCoefficient).toInt

  val requestMultiplier = 5
  val requestsPerToken: Map[String, Int] = mocks.map { case (token, sla) =>
      (token -> sla.maxRps * requestMultiplier * time)
    }

  val allRequests = requestsPerToken.map(_._2).sum

  sealed class SLAServiceTestImpl (implicit ec: ExecutionContext) extends SLAService {
    final val slaMocks = mocks
    override def getSlaByToken(token: String): Future[Sla] = Future {
      slaMocks(token)
    }
  }

  val testService = new SLAServiceTestImpl

  sealed class ThrottlingServiceTest (implicit ec: ExecutionContext)  extends ThrottlingService {
    override val GraceRps = 1000

    override def isRequestAllowed(token: Option[String]): Future[Boolean] = Future {
      true
    }

    override val slaService: SLAService = testService
  }

  val throttlingServiceMock = new ThrottlingServiceTest

  case class MyInterruptException(message: String = "", cause: Throwable = null) extends
    RuntimeException(message, cause)

  "Application" should {


    def generateRequests(globalTimeoutF : Option[Future[play.api.mvc.Result]]) = requestsPerToken.map { case (userToken, reqAmount) =>
      (1 to reqAmount).map { index =>
        val p = Promise[play.api.mvc.Result]()
        val timeout = (time.toDouble * 1000 / reqAmount * index).toInt
        scheduler.scheduleOnce(timeout.milliseconds) {
          val fakeReq = route(FakeRequest(GET,
            "/throttling",
            FakeHeaders(Seq(Application.AUTHORIZATION -> userToken)),
            AnyContentAsEmpty)).get
          p.completeWith(globalTimeoutF
            .map( globalF => Future.firstCompletedOf(Seq(fakeReq, globalF)))
            .getOrElse(fakeReq))
        }
        p.future
      }
    }

    def runAllRequest() = {
      lazy val globalTimeout = {
        val pTimeout = Promise[play.api.mvc.Result]()
        scheduler.scheduleOnce(time.seconds) {
          pTimeout failure(MyInterruptException()) // we should not count request that came later configured time
        }
        pTimeout.future
      }
      lazy val reqList = generateRequests(Some(globalTimeout))
      reqList.reduceLeft(_ ++ _).map( fRes => Try(status(fRes))).collect {
        case scala.util.Success(st) => st
      }
    }


    val application = new GuiceApplicationBuilder()
      .configure("throttling.auto_scale_percents" -> 0)
      .overrides(
        bind[SLAService].qualifiedWith("direct").toInstance(testService).eagerly()
      )

    "run the throttling page and count successful requests" in new WithApplication(app = application.build){
      val result = runAllRequest()
      result.count( _ == OK) must be_<=(maxRequestsPerTime)
    }

    "run the throttling page and count failed requests" in new WithApplication(app = application.build){
      val result = runAllRequest()
      result.count(_ == TOO_MANY_REQUESTS) must be_>=(result.size - maxRequestsPerTime)
    }


    "run the throttling page without throttling" in new WithApplication(app =
      application.overrides(
        bind[ThrottlingService].toInstance(throttlingServiceMock).eagerly()
      ).build){
      lazy val reqList = generateRequests(None)

      reqList.reduceLeft(_ ++ _).map( fRes => Try(status(fRes))).collect {
        case scala.util.Success(st) => st
      }.count( _ == OK) must be_==(allRequests)
    }
  }
}
